
TEX = luatex --file-line-error --shell-escape --lua=../minim-log.lua
EUPL = EUPL-1.2-EN.txt

XMP_DIR = xmp
XMP_FILES = minim-xmp.tex minim-xmp.lua minim-xmp.doc
PDF_DIR = pdf
PDF_FILES = minim-pdf.tex minim-pdf.lua minim-languagecodes.lua minim-pdf.doc
MATHS_DIR = math
MATHS_FILES = minim-math.tex minim-math.lua minim-math-table.lua minim-math.doc minim-math.sty
HATCH_DIR = hatching
HATCH_FILES = minim-hatching.mp minim-hatching-doc.mp
MP_DIR = mp
MP_FILES = minim.mp minim-mp.mp minim-mp.ini minim-lamp.ini minim-lamp.mp \
	minim-mp.tex minim-mp.lua minim-mp.doc minim-mp.sty
MAIN_DIR = base
MAIN_FILES = minim-etex.tex minim-plain.tex minim-lmodern.tex minim.tex minim.ini minim.doc \
	minim-alloc.tex minim-alloc.lua minim-callbacks.lua \
	minim-doc.sty minim-alloc.doc minim-pdfresources.lua minim-pdfresources.tex
LUA_FILES = ${XMP_DIR}/*.lua ${PDF_DIR}/*.lua ${MP_DIR}/*.lua ${MAIN_DIR}/*.lua ${PDF_DIR}/*.lua minim-log.lua

documentation : formats \
				${XMP_DIR}/minim-xmp.pdf \
				${PDF_DIR}/minim-pdf.pdf \
				${MATHS_DIR}/minim-math.pdf \
				${MP_DIR}/minim-mp.pdf \
				${HATCH_DIR}/minim-hatching-doc.pdf \
				${MAIN_DIR}/minim.pdf
	@echo "All manuals made."

check-standards : base/minim.pdf
	verapdf --format text -f 3a ${MAIN_DIR}/minim.pdf
	verapdf --format text -f ua1 ${MAIN_DIR}/minim.pdf

luacheck :
	luacheck ${LUA_FILES}

packages :	check-standards \
			minim-xmp.tar.gz \
			minim-pdf.tar.gz \
			minim-math.tar.gz \
			minim-mp.tar.gz \
			minim-hatching.tar.gz \
			minim.tar.gz \
			luacheck
	@./check-versions

formats : ${MAIN_DIR}/minim.fmt ${MP_DIR}/minim-mp.fmt ${MP_DIR}/minim-lamp.fmt

${MAIN_DIR}/minim.fmt : */*.tex */*.lua ${MAIN_DIR}/minim.ini
	cd ${MAIN_DIR}; luatex -ini minim.ini; mv minim.log minim-fmt.log
${MP_DIR}/minim-mp.fmt : ${MAIN_DIR}/minim.fmt ${MP_DIR}/minim-mp.ini
	cd ${MP_DIR}; luatex -ini minim-mp.ini; mv minim-mp.log minim-mp-fmt.log
${MP_DIR}/minim-lamp.fmt : ${MAIN_DIR}/minim.fmt ${MP_DIR}/minim-lamp.ini
	cd ${MP_DIR}; luatex -ini minim-lamp.ini; mv minim-lamp.log minim-lamp-fmt.log

clean:
	rm -f *.tar.gz *.log *.aux */*.log */*.aux */*.fmt

${XMP_DIR}/minim-xmp.pdf : $(addprefix ${XMP_DIR}/,${XMP_FILES}) ${MAIN_DIR}/minim-doc.sty
	cd ${XMP_DIR}; ${TEX} minim-xmp.doc
minim-xmp.tar.gz : ${XMP_DIR}/minim-xmp.pdf
	tar czf minim-xmp.tar.gz --transform='s/^LICENSE/minim-xmp\/${EUPL}/;s/^[^\/]*/minim-${XMP_DIR}/' \
		$(addprefix ${XMP_DIR}/,${XMP_FILES}) LICENSE ${XMP_DIR}/README ${XMP_DIR}/minim-xmp.pdf

${PDF_DIR}/minim-pdf.pdf : $(addprefix ${PDF_DIR}/,${PDF_FILES}) ${MAIN_DIR}/minim-doc.sty ${MAIN_DIR}/minim.fmt
	cd ${PDF_DIR}; ${TEX} minim-pdf.doc
minim-pdf.tar.gz : ${PDF_DIR}/minim-pdf.pdf
	tar czf minim-pdf.tar.gz --transform='s/^LICENSE/minim-pdf\/${EUPL}/;s/^[^\/]*/minim-${PDF_DIR}/' \
		$(addprefix ${PDF_DIR}/,${PDF_FILES}) LICENSE ${PDF_DIR}/README ${PDF_DIR}/minim-pdf.pdf

${MATHS_DIR}/minim-math.pdf : $(addprefix ${MATHS_DIR}/,${MATHS_FILES}) ${MAIN_DIR}/minim-doc.sty
	cd ${MATHS_DIR}; ${TEX} minim-math.doc
minim-math.tar.gz : ${MATHS_DIR}/minim-math.pdf
	tar czf minim-math.tar.gz --transform='s/^LICENSE/minim-math\/${EUPL}/;s/^[^\/]*/minim-math/' \
		$(addprefix ${MATHS_DIR}/,${MATHS_FILES}) LICENSE ${MATHS_DIR}/README ${MATHS_DIR}/minim-math.pdf

${MP_DIR}/minim-mp.pdf : $(addprefix ${MP_DIR}/,${MP_FILES}) ${MAIN_DIR}/minim-doc.sty ${MP_DIR}/minim-mp.fmt
	cd ${MP_DIR}; ${TEX} minim-mp.doc
minim-mp.tar.gz : ${MP_DIR}/minim-mp.pdf
	tar czf minim-mp.tar.gz --transform='s/^LICENSE/minim-mp\/${EUPL}/;s/^[^\/]*/minim-mp/' \
		$(addprefix ${MP_DIR}/,${MP_FILES}) LICENSE ${MP_DIR}/README ${MP_DIR}/minim-mp.pdf

${HATCH_DIR}/minim-hatching-doc.pdf : hatching $(addprefix ${HATCH_DIR}/,${HATCH_FILES}) ${MAIN_DIR}/minim-doc.sty ${MP_DIR}/minim-mp.fmt
	cd ${HATCH_DIR}; ${TEX} --fmt=../${MP_DIR}/minim-mp minim-hatching-doc.mp
minim-hatching.tar.gz : ${HATCH_DIR}/minim-hatching-doc.pdf
	tar czf minim-hatching.tar.gz --transform='s/^LICENSE/minim-hatching\/${EUPL}/;s/^[^\/]*/minim-${HATCH_DIR}/' \
		$(addprefix ${HATCH_DIR}/,${HATCH_FILES}) LICENSE ${HATCH_DIR}/README ${HATCH_DIR}/minim-hatching-doc.pdf

${MAIN_DIR}/minim.pdf : $(addprefix ${MAIN_DIR}/,${MAIN_FILES}) */*.doc ${MAIN_DIR}/minim-doc.sty
	cd ${MAIN_DIR}; ${TEX} minim.doc
	cd ${MAIN_DIR}; ${TEX} --fmt=minim minim.doc
	pdfinfo -struct ${MAIN_DIR}/minim.pdf 2>/dev/null > ${MAIN_DIR}/minim.struct
minim.tar.gz : ${MAIN_DIR}/minim.pdf
	tar czf minim.tar.gz --transform='s/^LICENSE/minim\/${EUPL}/;s/^[^\/]*/minim/' \
		$(addprefix ${MAIN_DIR}/,${MAIN_FILES}) LICENSE ${MAIN_DIR}/README ${MAIN_DIR}/minim.pdf

TEXMFHOME ?= ~/texmf
LOCALMP = ${TEXMFHOME}/metapost/minim
LOCALTEX = ${TEXMFHOME}/luatex/minim
LOCALWEB2C = ${TEXMFHOME}/web2c/luatex
INSTALL :
	mkdir -p ${LOCALMP} ${LOCALTEX} ${LOCALWEB2C}
	cp */*.mp ${LOCALMP}
	cp */*.{tex,lua,doc,ini} ${LOCALTEX}
	cd ${LOCALWEB2C}; luatex -ini minim.ini
	cd ${LOCALWEB2C}; luatex -ini minim-mp.ini
	cd ${LOCALWEB2C}; luatex -ini minim-lamp.ini

