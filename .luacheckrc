
codes = true
std = 'lua53+luatex'
ignore =
    { '212' -- unused argument
    , '542' -- empty if branch
    , '614' -- trailing spaces in comment
    , '631' -- line too long
    }
stds.luatex = {
    globals = 
        { 'callback'
        , 'font'
        , 'fonts'
        , 'kpse'
        , 'lua'
        , 'luaotfload'
        , 'luatexbase'
        , 'mplib'
        , 'node'
        , 'pdf'
        , 'status'
        , 'tex'
        , 'texio'
        , 'texconfig'
        , 'token'
        },
}

