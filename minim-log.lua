
--[[ minim-log.lua : lua init script for producing cleaner logs
--
-- This script, invoked on the luatex command-line with
--
--     --lua=/path/to/minim-log.lua
--
-- inserts a few callbacks that should produce less and easier-to-read logs on 
-- your terminal. It currently does the following:
--
--  • Log line length is set tot 1024;
--  • The log banner reports the name of the input file, and the jobname;
--  • Open files are only written to the log, and always start a new line;
--  • Error and over/underfull messages display a bit more detail about the 
--    context;
--  • Error messages and warnings are marked with colour (in the terminal 
--    only);
--  • A hyphenation report is printed at the end of the log, listing all 
--    hyphenated words per language used.
--
--]]

texconfig.max_print_line = 1024
texconfig.file_line_error = true

-- 1 auxiliary functions

local HLIST = node.id 'hlist'
local KERN = node.id 'kern'
local DISC = node.id 'disc'
local CHAR = node.id 'glyph'
local GLUE = node.id 'glue'

--local function wlog(...) texio.write('log', string.format(...)) end
local function wlog_nl(...) texio.write_nl('log', string.format(...)) end
local function wlog(...) texio.write('log', string.format(...)) end
local function wterm(...) texio.write('term', string.format(...)) end
local function wterm_nl(...) texio.write_nl('term', string.format(...)) end
local function wboth(...) texio.write('term and log', string.format(...)) end
local function wboth_nl(...) texio.write_nl('term and log', string.format(...)) end

-- 1 banner

local escape_modes = { [0] = 'disabled', 'enabled', 'restricted' }

local function start_run()
    texio.setescape(false)
    wterm('\027[0;32m')
    wboth('%s; shell escape %s', status.banner, escape_modes[status.shell_escape])
    wterm_nl('\027[0m')
    texio.setescape(true)
end

callback.register('start_run', start_run)

-- 1 file reporting

local filetypes_left = { '(', '{', '<', '<', '<<' }
local filetypes_right = { ')', '}', '>', '>', '>>' }
local first_file = true

local function start_file(category, filename)
    if first_file then
        first_file = false
        texio.setescape(false)
        wterm('\027[0;32m')
        wboth('    %s -> \\jobname = {%s}', filename, tex.jobname)
        wterm('\027[0m')
        texio.setescape(true)
        wterm_nl ''
    end
    wlog_nl('%s%s', filetypes_left[category], filename)
end
local function stop_file(category)
    wlog(filetypes_right[category])
end

callback.register('start_file', start_file)
callback.register('stop_file', stop_file)

-- 1 error context

local grouptypes = { -- equivalents.h
    [0] = 'bottom_level', 'simple', 'hbox', 'adjusted_hbox', 'vbox', 'vtop',
    'align', 'no_align', 'output', 'math', 'disc', 'insert', 'vcenter',
    'math_choice', 'semi_simple', 'math_shift', 'math_left', 'local_box',
    'split_off', 'split_keep', 'preamble', 'align_set', 'fin_row',
}

local iftypes = { -- conditional.h
    'if', 'ifcat', 'ifnum', 'ifdim', 'ifodd', 'ifvmode', 'ifhmode', 'ifmmode',
    'ifinner', 'ifvoid', 'ifhbox', 'ifvbox', 'ifx', 'ifeof', 'iftrue', 'iffalse',
    'ifcase', 'ifdefined', 'ifcsname', 'iffontchar', 'ifincsname', 'ifprimitive',
    'ifabsnum', 'ifabsdim', 'ifcondition',
}

local branchtypes = { [1] = 'then', [-1] = 'else', [0] = 'undecided' }

local function getiftype()
    local cit = tex.currentiftype
    if cit < 0 then
        return 'unless|'..iftypes[-cit]
    else
        return iftypes[cit]
    end
end

local function show_error_hook()
    wboth_nl('Page %d, group level %d (%s)',
        tex.count[0], tex.currentgrouplevel, grouptypes[tex.currentgrouptype])
    if tex.currentiflevel > 0 then
        wboth(', if level %d (%s/%s)',
            tex.currentiflevel, getiftype(),
            branchtypes[tex.currentifbranch])
    end
    if status.output_active then wboth(' (during \\output)') end
    wboth_nl('%s', status.lasterrorcontext)
end

callback.register('show_error_hook', show_error_hook)

-- 1 error messages

local function show_error_message()
    local msg = status.lasterrorstring
    texio.setescape(false)
    wterm('\n\027[0;31m')
    wboth_nl('%s', msg)
    wterm('\027[0m')
    texio.setescape(true)
end

callback.register('show_error_message', show_error_message)

local function show_lua_error_hook()
    local msg = status.lastluaerrorstring
    texio.setescape(false)
    wterm('\n\027[0;31m')
    wboth_nl('%s', msg)
    wterm('\027[0m')
    texio.setescape(true)
end

callback.register('show_lua_error_hook', show_lua_error_hook)

-- 1 overfull warnings

local function hpack_quality(incident, detail, head, first, last)
    if incident == 'overfull' then
        detail = string.format('by %.1fpt', detail/65536)
    else
        detail = string.format('badness %.0f', detail)
    end
    texio.setescape(false)
    wterm('\027[0;33m')
    wboth_nl('\\hbox[%.1fpt] %s (%s) at page %d (%s:%d)', head.width/65536,
        incident, detail, tex.count[0], status.filename, status.linenumber)
    wterm('\027[0m\n')
    texio.setescape(true)
    for n in node.traverse(head.list) do
        if n.char then
            wboth('%s', utf8.char(n.char))
        elseif n.id == GLUE and n.width > 100 then
            wboth(' ')
        elseif n.id == DISC then
            wboth('-')
        end
    end
end

callback.register('hpack_quality', hpack_quality)

local function vpack_quality(incident, detail, head, first, last)
    if incident == 'overfull' then
        detail = string.format('by %.1fpt', detail/65536)
    else
        detail = string.format('badness %.0f', detail)
    end
    texio.setescape(false)
    wterm('\027[0;33m')
    wboth_nl('\\vbox[%.1fpt+%.1fpt] %s (%s) at page %d (%s:%d)',
        head.height/65536, head.depth/65536,
        incident, detail, tex.count[0], status.filename, status.linenumber)
    wterm('\027[0m\n')
    texio.setescape(true)
end

callback.register('vpack_quality', vpack_quality)

-- 1 hyphenation report

local hyphenations = { }

local function post_linebreak_filter(head, groupcode)
    local first_half -- last word on previous line
    for line in node.traverse_id(HLIST, head) do
        local cur_word, disc, lang, last_word = { }, false, nil, nil
        for n in node.traverse(line.head) do
            -- take language from the first character
            if not lang and n.lang then lang = n.lang end
            if n.components then
                for nn in node.traverse_id(CHAR, n.components) do
                    table.insert(cur_word, nn.char)
                end
            elseif n.char then
                table.insert(cur_word, n.char)
            elseif n.id ~= KERN and n.id ~= DISC and #cur_word>0 then
                cur_word = utf8.char(table.unpack(cur_word))
                if first_half and not last_word then
                    -- found a hyphenation
                    local hyphenated = first_half .. cur_word
                    hyphenated = string.gsub(hyphenated, '[.,?!:;;¡¿]', '')
                    lang = lang or -1
                    hyphenations[lang] = hyphenations[lang] or { }
                    hyphenations[lang][hyphenated] = true
                end
                last_word, cur_word, lang = cur_word, { }, nil
                first_half = disc and last_word
            end
            -- exclude explicit hyphens (subtype 2)
            disc = n.id == DISC and n.subtype ~= 2
        end
    end
    return true
end

callback.register('post_linebreak_filter', post_linebreak_filter)

local function finish_pdffile()
    for lang, list in pairs(hyphenations) do
        texio.setescape(false)
        wterm('\027[0;33m')
        wboth_nl('Hyphenations for language %d: ', lang)
        wterm('\027[0m')
        texio.setescape(true)
        local sorted = { }
        for h, _ in pairs(list) do
            table.insert(sorted, h)
        end
        table.sort(sorted)
        for _, h in ipairs(sorted) do
            wboth_nl('    %s', h)
        end
    end
end

callback.register('finish_pdffile', finish_pdffile)

-- 


