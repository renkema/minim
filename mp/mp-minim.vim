
" This is a partial syntax file for VIM. It builds upon the syntax file 
" included with your VIM installation – I hope that file is the same for you 
" and me!

" HOW TO INSTALL:
"
" 1) Navigate to `$XDG_CONFIG_HOME/vim/after/syntax/`
" 2) Create a link to this file (or copy it over)
" 3) Create the file `mp.vim` with the following contents:
"       runtime! syntax/mp-minim.vim
" 4) You can add your own macros and settings in the same file, or in separate 
"    files included in the same way.


" For some reason the default syntax file only enables these for metafun
syn keyword mpPrimitive runscript maketext
" These are missed completely
syn keyword mpPrimitive prescriptpart postscriptpart

" The following are are from minim-mp.mp

syn keyword mpDef glyph contours luafunction
syn keyword mpDef make_lua_dict append_postscript debug_pdf
syn keyword mpDef savegstate restoregstate setgstate withgstate withalpha
syn keyword mpDef nofill eofill eofilldraw multidraw multifill multifilldraw multieofill multieofilldraw
syn keyword mpDef beginpattern endpattern withpattern

syn match mpVardef "\<\%(set[ .]\)\?tex[. ]\%(count\|attribute\|dimen\|toks\|skip\)\>"
syn keyword mpVardef texmessage baseline find_baseline
syn keyword mpVardef luavariable setluavariable index_or_suffix
syn keyword mpVardef hexadecimal scaledpoints quote_for_lua lua_string
syn keyword mpVardef boxresource saveboxresource setalpha
syn match mpVardef 'transparent'

syn keyword mpNewInternal blend_mode transparency_group_attrs tilingtype
syn keyword mpVariable debug_tex_bboxes maketextlabels defaultfont

" These are from minim.mp

syn keyword mpDef hflip vflip typeof
syn keyword mpDef save_pen save_pair save_path save_color save_cmykcolor
syn keyword mpDef save_transform save_picture save_boolean save_string

syn keyword mpPrimaryDef xshifted yshifted extprod

syn keyword mpVardef clipto clipout clockwise collinear
syn keyword mpVardef getbounds empty rgb_to_gray
syn keyword mpVardef tand arcsind arccosd arctand arc

syn keyword mpConstant cyan magenta yellow key
syn keyword mpConstant fullsquare unitcircle pi

